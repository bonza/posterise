# About

Posterisation of RGB images. Uses K-Means Cluster Analysis to identify a 
user-specified number of colour clusters within the image, and maps each 
original pixel in the image to its associated colour cluster centre, returning
a modified (posterised) image.

# Usage

Requires the user to specify the path of the RGB image to be processed, the
path of the resulting posterised image, and the number of colours in the posterised
image. It can be run in the terminal as follows:

```
python posterise.py /path/to/original.png /path/to/posterised.png 8
```

The above command produces the image posterised.png which consists of eight colours.

# Dependencies

This package uses scikit-learn's K-means clustering capabilities to perform the
posterisation - you will need the `sklearn` package. 