# -*- coding: utf-8 -*-
"""
Posterises an RGB image using K-Means Cluster Analysis to identify a
user-specified number of colour clusters within the image. 

Usage:
    posterise.py  <img_path> <dest_path> <n_clusters>
    posterise.py  -h | --help
    
Options:
    -h --help        Show this screen. 
    
"""

from docopt import docopt
import imageio
import numpy as np
from sklearn.cluster import KMeans
import warnings


def generate_image_array(img_path, n_clusters):
    """
    Converts an image file to a set of RGB vector samples, runs cluster 
    analysis on these samples, maps each sample to it's identified cluster 
    centre (in RGB space)), and reshapes the mapped sample set back to an 
    (n, m, 3) array (the shape of the original image in RGB space).
    
    Args:
        img_path (str): location of the original image file.
        n_clusters (int): number of colours original image will be reduced to.
        
    Returns:
        poster(numpy.array): RGB array representation of the original image, 
        where each point `n_clusters` identified colour clusters.
            
    """
    # read in image file as three dimensional numpy array
    img = imageio.imread(img_path)
    shape = img.shape
    new_shape = (shape[0] * shape[1], 3)
    # reshape to (n * m, 3)
    img = np.reshape(img, new_shape)
    # do cluster analysis on each pixel (point)
    kmeans = KMeans(n_clusters=n_clusters, n_init=2, n_jobs=2)
    kmeans.fit(img)
    labels = kmeans.labels_
    # round cluster centre coordinates to nearest int, as RGB is discrete
    centres = np.round(kmeans.cluster_centers_).astype(int)
    # map original points to their cluster centres
    poster = np.zeros(new_shape, dtype=int)
    for i in range(new_shape[0]):
        label = labels[i]
        centre = centres[label]
        poster[i] = centre
    # convert back to original shape 
    poster = np.reshape(poster, shape)
    return poster


def write_image(poster, dest_path):
    """
    Writes an RGB array to an image file.
    
    Args:
        poster (numpy.array): RGB array to be converted to an image. Must be of 
            shape (n, m, 3), where n and m represent the vertical and 
            horizontal pixel sizes of the image respectively, and the third 
            dimension represents the reg, green and blue channels of the image.
        dest_path (str): the path (including image file extension) where the 
            image will be written to.
            
    """
    imageio.imwrite(dest_path, poster)


if __name__ == "__main__":
    warnings.filterwarnings("ignore")
    args = docopt(__doc__)
    poster = generate_image_array(args["<img_path>"], int(args["<n_clusters>"]))
    write_image(poster, args["<dest_path>"])
    